// Shoot Them Up Game. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Weapon/STUBaseWeapon.h"
#include "STULauncherBaseWeapon.generated.h"

/**
 *
 */

class ASTUProjectile;
UCLASS()
class SHOOTTHEMUP_API ASTULauncherBaseWeapon : public ASTUBaseWeapon {
    GENERATED_BODY()

  public:
    virtual void StartFire() override;

  protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
    TSubclassOf<ASTUProjectile> ProjectileClass;

    virtual void MakeShot() override;
};
