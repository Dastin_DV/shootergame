// Shoot Them Up Game. All rights reserved

#pragma once

#include "Animation/AnimNotifies/AnimNotify.h"
#include "Animations/STUAnimNotify.h"
#include "CoreMinimal.h"
#include "STUEquipFinishedAnimNotify.generated.h"
/**
 *
 */
UCLASS()
class SHOOTTHEMUP_API USTUEquipFinishedAnimNotify : public USTUAnimNotify {
    GENERATED_BODY()


};
