// Shoot Them Up Game. All rights reserved

#pragma once

#include "Components/SphereComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "STUBasePickup.generated.h"

class USphereComponent;

UCLASS()
class SHOOTTHEMUP_API ASTUBasePickup : public AActor {
    GENERATED_BODY()

  public:
    // Sets default values for this actor's properties
    ASTUBasePickup();

  protected:
    UPROPERTY(EditDefaultsOnly, Category = "Pickup")
    USphereComponent *CollisionComponent;

    UPROPERTY(EditDefaultsOnly, Category = "Pickup")
    float RespawnTime = 5.0f;
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;
    virtual void NotifyActorBeginOverlap(AActor *OtherActor) override;

  public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;

  private:
    float RotationYaw = 0.0f;

    virtual bool GivePickupTo(APawn* PlayerPawn);

    void PickupWasTaken();
    void Respawn();
    void GenerateRotationYaw();
};
