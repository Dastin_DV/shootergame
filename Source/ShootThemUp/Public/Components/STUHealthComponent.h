// Shoot Them Up Game. All rights reserved

#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "Engine/EngineTypes.h"
#include "STUCoreTypes.h"
#include "STUHealthComponent.generated.h"

class UCameraShakeBase;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class SHOOTTHEMUP_API USTUHealthComponent : public UActorComponent {
    GENERATED_BODY()

  public:
    // Sets default values for this component's properties
    USTUHealthComponent();

    float GetHealth() const {
        return Health;
    };

    UFUNCTION(BlueprintCallable, Category = "Health")
    bool IsDead() const {
        return Health <= 0.0f;
    };

    UFUNCTION(BlueprintCallable, Category = "Health")
    float GetHealthPercent() const {
        return Health / MaxHealth;
    };

    bool AddHealth(float HealthAmount);

    FOnDeath OnDeath;
    FOnHealthChanged OnHealthChanged;
    FOnHit OnHit;

  protected:
    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health", meta = (ClampMin = "0.0", ClampMax = "1000.0"))
    float MaxHealth = 100.0f;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Heal", meta = (EditCondition = "AutoHeal"))
    float RecoverVal = 2.0f;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Heal", meta = (EditCondition = "AutoHeal"))
    float Delay = 3.0f;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Heal", meta = (EditCondition = "AutoHeal"))
    float UpdateTime = 1.0f;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Heal")
    bool AutoHeal = true;

    UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Heal")
    TSubclassOf<UCameraShakeBase> CameraShake;

    // Called when the game starts
    virtual void BeginPlay() override;

  private:
    float Health = 0.0f;
    float LastHealthValue = 0.0f;
    FTimerHandle HealthTimerHandle;
    UFUNCTION()
    void OnTakeAnyDamage(AActor *DamagedActor, float Damage, const class UDamageType *DamageType,
                         class AController *InstigatedBy, AActor *DamageCauser);

    void OnHealthRecover();
    void PlayCameraShake();
    bool IsHealthFull() const;
};
