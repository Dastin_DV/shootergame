// Shoot Them Up Game. All rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "STUMyGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTTHEMUP_API ASTUMyGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
	public:
      ASTUMyGameModeBase();
};
