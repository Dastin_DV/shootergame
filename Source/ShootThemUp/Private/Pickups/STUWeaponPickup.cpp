// Shoot Them Up Game. All rights reserved


#include "Pickups/STUWeaponPickup.h"
#include "Components/STUHealthComponent.h"
#include "Components/STUWeaponComponent.h"
#include "STUUtils.h"

DEFINE_LOG_CATEGORY_STATIC(LogWeaponPickup, All, All);

bool ASTUWeaponPickup::GivePickupTo(APawn *PlayerPawn) {
    const auto HealthComponent = STUUtils::GetSTUPlayerComponent<USTUHealthComponent>(PlayerPawn);
    if (!HealthComponent || HealthComponent->IsDead())
        return false;

    const auto WeaponComponent = STUUtils::GetSTUPlayerComponent<USTUWeaponComponent>(PlayerPawn);
    if (!WeaponComponent)
        return false;

    UE_LOG(LogWeaponPickup, Display, TEXT("Weapon!"));
    return WeaponComponent->TryToAddAmmo(WeaponType, ClipsAmount);
}