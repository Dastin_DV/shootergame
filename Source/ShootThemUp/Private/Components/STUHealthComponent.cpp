// Shoot Them Up Game. All rights reserved

#include "Components/STUHealthComponent.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/Controller.h"
#include "TimerManager.h"
#include "Camera/CameraShake.h"

DEFINE_LOG_CATEGORY_STATIC(LogHealthComponent, All, All);

// Sets default values for this component's properties
USTUHealthComponent::USTUHealthComponent() {
    // Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these
    // features off to improve performance if you don't need them.
    PrimaryComponentTick.bCanEverTick = false;

    // ...
}

// Called when the game starts
void USTUHealthComponent::BeginPlay() {
    Super::BeginPlay();
    check(MaxHealth > 0);

    //LastHealthValue = 0.0f;
    Health = MaxHealth;

    //const auto HealthDelta = Health - LastHealthValue;
    OnHealthChanged.Broadcast(Health, 0.0f);

    AActor *ComponentOwner = GetOwner();

    if (ComponentOwner) {
        ComponentOwner->OnTakeAnyDamage.AddDynamic(this, &USTUHealthComponent::OnTakeAnyDamage);
    }
}

void USTUHealthComponent::OnTakeAnyDamage(AActor *DamagedActor, float Damage, const UDamageType *DamageType,
                                          AController *InstigatedBy, AActor *DamageCauser) {
    if (Damage <= 0.0f || IsDead())
        return;
    LastHealthValue = Health;
    Health = FMath::Clamp(Health - Damage, 0.0f, MaxHealth);

    GetWorld()->GetWorldSettings()->GetWorldTimerManager().SetTimer(
        HealthTimerHandle, this, &USTUHealthComponent::OnHealthRecover, UpdateTime, true, Delay);

    const auto HealthDelta = Health - LastHealthValue;
    OnHealthChanged.Broadcast(Health, HealthDelta);

    if (IsDead()) {
        GetWorld()->GetWorldSettings()->GetWorldTimerManager().ClearTimer(HealthTimerHandle);
        OnDeath.Broadcast();
    }
    PlayCameraShake();
}

void USTUHealthComponent::OnHealthRecover() {
    if (IsDead() && !AutoHeal)
        return;

    LastHealthValue = Health;
    Health = FMath::Clamp(Health + RecoverVal, 0.0f, MaxHealth);
    if (Health == MaxHealth) {
        GetWorld()->GetWorldSettings()->GetWorldTimerManager().ClearTimer(HealthTimerHandle);
    }
    const auto HealthDelta = Health - LastHealthValue;
    OnHealthChanged.Broadcast(Health, HealthDelta);
}

void USTUHealthComponent::PlayCameraShake() {
    if (IsDead())
        return;

    const auto Player = Cast<APawn>(GetOwner());
    if (!Player)
        return;

    const auto Controller = Player->GetController<APlayerController>();
    if (!Controller || !Controller->PlayerCameraManager)
        return;

    Controller->PlayerCameraManager->StartCameraShake(CameraShake);
}


bool USTUHealthComponent::AddHealth(float HealthAmount) {
    if (IsHealthFull())
        return false;

    Health = FMath::Min(Health + HealthAmount, MaxHealth);
    return true;
}

bool USTUHealthComponent::IsHealthFull() const {
    return Health == MaxHealth;
}