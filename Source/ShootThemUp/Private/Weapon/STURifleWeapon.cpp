// Shoot Them Up Game. All rights reserved

#include "Weapon/STURifleWeapon.h"
#include "Components/STUHealthComponent.h" //
#include "DrawDebugHelpers.h"
#include "Engine/World.h"
#include "Player/STUBaseCharacter.h"
#include "Weapon/Components/STUWeaponFXComponent.h"

ASTURifleWeapon::ASTURifleWeapon() {
    WeaponFXComponent = CreateDefaultSubobject<USTUWeaponFXComponent>("STUWeaponFXComponent");
}

//void ASTURifleWeapon::BeginPlay() {
//    //check(WeaponFXComponent);
//}

void ASTURifleWeapon::StartFire() {
    GetWorldTimerManager().SetTimer(ShotTimerHandle, this, &ASTURifleWeapon::MakeShot, TimeBetweenShots, true);
    MakeShot();
}

void ASTURifleWeapon::StopFire() {
    GetWorldTimerManager().ClearTimer(ShotTimerHandle);
}

void ASTURifleWeapon::MakeShot() {
    if (!GetWorld() || IsAmmoEmpty()) {
        StopFire();
        return;
    }
    FVector TraceStart, TraceEnd;
    if (!GetTraceData(TraceStart, TraceEnd)) {
        StopFire();
        return;
    }
    FHitResult HitResult;
    MakeHit(HitResult, TraceStart, TraceEnd);

    DecreaseAmmo(); // ��������� �������.
    if (HitResult.bBlockingHit) {
        //DrawDebugLine(GetWorld(), GetMuzzleWorldLocation(), HitResult.ImpactPoint, FColor::Red, false, 3.0f, 0, 3.0f);
       // DrawDebugSphere(GetWorld(), HitResult.ImpactPoint, 10.0f, 24, FColor::Red, false, 5.0f);
        WeaponFXComponent->PlayImpactFX(HitResult);
        const ASTUBaseCharacter *Victim = Cast<ASTUBaseCharacter>(HitResult.GetActor());
        if (!Victim)
            return;
        Victim->getHealthComponent()->OnHit.Broadcast();
    } else {
        DrawDebugLine(GetWorld(), GetMuzzleWorldLocation(), TraceEnd, FColor::Red, false, 3.0f, 0, 3.0f);
    }

    // DecreaseAmmo();     // ��������� �������.
}

bool ASTURifleWeapon::GetTraceData(FVector &TraceStart, FVector &TraceEnd) const {
    FVector ViewLocation;
    FRotator ViewRotation;
    if (!GetPlayerViewPoint(ViewLocation, ViewRotation))
        return false;

    TraceStart = ViewLocation;
    const auto HalfRad = FMath::DegreesToRadians(BulletSpread);
    const FVector ShootDirection = FMath::VRandCone(ViewRotation.Vector(), HalfRad);
    TraceEnd = TraceStart + ShootDirection * TraceMaxDistance;
    return true;
}